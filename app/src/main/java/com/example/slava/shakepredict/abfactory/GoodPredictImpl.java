package com.example.slava.shakepredict.abfactory;

import com.example.slava.shakepredict.modelsdb.PredictDb;

import java.util.List;
import java.util.Random;


public class GoodPredictImpl implements PredictInterface {
    @Override
    public String createPredict() {
        List<PredictDb> jokeDbList = PredictDb.getPredictByType("good");
        return jokeDbList.get(new Random().nextInt(jokeDbList.size())).predict;
    }
}
