package com.example.slava.shakepredict.abfactory;


import android.util.Log;

public class FactoryDirector {

    public String createFactory(String factory, String infoType) {
        String info = null;
        GiveInfoAbstractFactory giveInfoAbstractFactory = null; //Инициализируем фабрику
        if(factory.equals("good"))
            giveInfoAbstractFactory = new GoodInfoFactory(); // Создаем фабрику для Хороших новостей
        if(factory.equals("bad"))
            giveInfoAbstractFactory = new BadInfoFactory(); // Создаем фабрику для Плохих новостей

        if(giveInfoAbstractFactory != null){
            switch (infoType) {
                case "Fact":
                    FactInterface fact = giveInfoAbstractFactory.getFact();
                    info = fact.sayFact();
                    break;
                case "Joke":
                    JokeInterface joke = giveInfoAbstractFactory.getJoke();
                    info = joke.makeJoke();
                    break;
                case "Predict":
                    PredictInterface predict = giveInfoAbstractFactory.getPredict();
                    info = predict.createPredict();
                    break;
            }
        }else{
            showLogs("Wrong type");
            info = "error";
        }

        return info;

    }

    private void showLogs(String msg){
        Log.d("Fabric", msg);
    }
}
