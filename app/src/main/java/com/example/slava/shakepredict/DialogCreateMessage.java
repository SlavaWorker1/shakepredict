package com.example.slava.shakepredict;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DialogCreateMessage extends AlertDialog.Builder {

    public DialogCreateMessage(final Context context) {
        super(context);
        final DateFormat mDateFormat = new SimpleDateFormat("HH:mm:ss");
        LayoutInflater inflater = ((Activity)context).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_message, null);
        final EditText edMessage = (EditText) dialogView.findViewById(R.id.edMessage);
        final EditText edName = (EditText) dialogView.findViewById(R.id.edName);
        final EditText edCompanion = (EditText) dialogView.findViewById(R.id.edCompanion);
        this.setView(dialogView)
                .setCancelable(false)
                .setPositiveButton("Cоздать сообщение",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialog, int which) {
                                if(edMessage.getText().toString().isEmpty()) {
                                    Toast.makeText(context, "Ошибка", Toast.LENGTH_SHORT).show();
                                } else {
                                    Message message = new Message.MessageBuilder(edMessage.getText().toString())
                                            .author(edName.getText().toString())
                                            .companion(edCompanion.getText().toString())
                                            .date(mDateFormat.format(new Date()))
                                            .build();
                                    ((MainActivity) context).getMessageList().add(message);
                                }

                            }
                        }
                )
                .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        final AlertDialog alert = this.create();
        alert.show();
    }
}
