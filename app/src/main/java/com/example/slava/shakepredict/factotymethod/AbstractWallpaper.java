package com.example.slava.shakepredict.factotymethod;


import android.widget.RelativeLayout;

public abstract class AbstractWallpaper {
    public abstract void setWallpaper(RelativeLayout relativeLayout);
}
