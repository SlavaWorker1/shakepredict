package com.example.slava.shakepredict.modelsdb;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;


@Table(name = "predict", id = "_id")
public class PredictDb extends Model {

    @Column(name = "predict")
    public String predict;

    @Column(name = "type")
    public String type;

    public PredictDb() {

    }

    public static List<PredictDb> getPredictByType(String type) {
        return new Select()
                .from(PredictDb.class)
                .where("type = ?", type)
                .execute();
    }



}
