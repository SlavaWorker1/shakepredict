package com.example.slava.shakepredict.abfactory;

import com.example.slava.shakepredict.modelsdb.JokeDb;

import java.util.List;
import java.util.Random;


public class BadJokeImpl implements JokeInterface {

    @Override
    public String makeJoke() {
        List<JokeDb> jokeDbList = JokeDb.getJokebyType("bad");
        return jokeDbList.get(new Random().nextInt(jokeDbList.size())).joke;
    }
}
