package com.example.slava.shakepredict.abfactory;

import com.example.slava.shakepredict.modelsdb.PredictDb;

import java.util.List;
import java.util.Random;


public class BadPredictImpl implements PredictInterface {
    @Override
    public String createPredict() {
        List<PredictDb> jokeDbList = PredictDb.getPredictByType("bad");
        return jokeDbList.get(new Random().nextInt(jokeDbList.size())).predict;
    }
}
