package com.example.slava.shakepredict.abfactory;

import com.example.slava.shakepredict.modelsdb.FactDb;

import java.util.List;
import java.util.Random;

/**
 * Created by anastasiia on 18.04.16.
 */
public class BadFactImpl implements FactInterface {
    @Override
    public String sayFact() {
        List<FactDb> factDbList = FactDb.getFactByType("bad");
        return factDbList.get(new Random().nextInt(factDbList.size())).fact;
    }
}
