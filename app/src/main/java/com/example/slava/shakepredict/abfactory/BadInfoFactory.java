package com.example.slava.shakepredict.abfactory;


public class BadInfoFactory extends GiveInfoAbstractFactory {
    @Override
    public FactInterface getFact() {
        return  new BadFactImpl();
    }

    @Override
    public JokeInterface getJoke() {
        return new BadJokeImpl();
    }

    @Override
    public PredictInterface getPredict() {
        return new BadPredictImpl();
    }
}
