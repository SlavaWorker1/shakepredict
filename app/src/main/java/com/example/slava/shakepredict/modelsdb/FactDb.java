package com.example.slava.shakepredict.modelsdb;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "fact", id = "_id")
public class FactDb extends Model {

    public FactDb() {
    }

    @Column (name = "fact")
    public String fact;

    @Column (name = "type")
    public String type;

    public static List<FactDb> getFactByType(String type) {
        return new Select()
                .from(FactDb.class)
                .where("type = ?", type)
                .execute();
    }


}
