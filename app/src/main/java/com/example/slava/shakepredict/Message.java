package com.example.slava.shakepredict;


public class Message {
    private final String message; // обязательное поле
    private final String author;
    private final String companion;
    private final String data;

    public Message(MessageBuilder builder) {
        this.message = builder.message;
        this.author = builder.author;
        this.companion = builder.companion;
        this.data = builder.data;
    }

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

    public String getCompanion() {
        return companion;
    }

    public String getData() {
        return data;
    }

    public static class MessageBuilder {
        private final String message; // обязательное поле
        private String author;
        private String companion;
        private String data;

        public MessageBuilder(String message) {
            this.message = message;
        }

        public MessageBuilder author(String author) {
            this.author = author;
            return this;
        }

        public MessageBuilder companion(String companion) {
            this.companion = companion;
            return  this;
        }

        public MessageBuilder date(String date) {
            this.data = date;
            return  this;
        }

        public Message build() {
            return new Message(this);
        }
    }


}
