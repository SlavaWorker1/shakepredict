package com.example.slava.shakepredict.abfactory;

import com.example.slava.shakepredict.modelsdb.JokeDb;

import java.util.List;
import java.util.Random;


public class GoodJokeImpl implements JokeInterface {
    @Override
    public String makeJoke() {
        List<JokeDb> jokeDbList = JokeDb.getJokebyType("good");
        return jokeDbList.get(new Random().nextInt(jokeDbList.size())).joke;
    }
}
