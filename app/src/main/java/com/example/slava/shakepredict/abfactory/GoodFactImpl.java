package com.example.slava.shakepredict.abfactory;

import com.example.slava.shakepredict.modelsdb.FactDb;

import java.util.List;
import java.util.Random;


public class GoodFactImpl implements FactInterface {
    @Override
    public String sayFact() {
        List<FactDb> factDbList = FactDb.getFactByType("good");
        return factDbList.get(new Random().nextInt(factDbList.size())).fact;
    }
}
