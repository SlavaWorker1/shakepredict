package com.example.slava.shakepredict.factotymethod;


public class FactoryMethod {
    public AbstractWallpaper getWallppaperColor(String wallpaperType) {
        AbstractWallpaper wallpaper= null;
            if(wallpaperType.equals("Lime")) {
                wallpaper = new LimeWallpaper();
            } else if (wallpaperType.equals("Light green")) {
                wallpaper = new LightGreenWallpaper();
            }
        return wallpaper;
    }
}
