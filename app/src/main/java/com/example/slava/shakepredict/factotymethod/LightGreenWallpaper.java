package com.example.slava.shakepredict.factotymethod;

import android.graphics.Color;
import android.widget.RelativeLayout;


public class LightGreenWallpaper extends AbstractWallpaper {
    @Override
    public void setWallpaper(RelativeLayout relativeLayout) {
        relativeLayout.setBackgroundColor(Color.parseColor("#8BC34A"));
    }
}
