package com.example.slava.shakepredict.abfactory;


public class GoodInfoFactory extends GiveInfoAbstractFactory {
    @Override
    public FactInterface getFact() {
        return new GoodFactImpl();
    }

    @Override
    public JokeInterface getJoke() {
        return new GoodJokeImpl();
    }

    @Override
    public PredictInterface getPredict() {
        return new GoodPredictImpl();
    }
}
