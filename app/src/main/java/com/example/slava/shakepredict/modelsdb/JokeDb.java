package com.example.slava.shakepredict.modelsdb;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.util.List;

@Table(name = "joke", id = "_id")
public class JokeDb extends Model {

    public JokeDb() {
    }

    @Column(name = "joke")
    public String joke;

    @Column(name = "type")
    public String type;

    public static List<JokeDb> getJokebyType(String type) {
        return new Select()
                .from(JokeDb.class)
                .where("type = ?", type)
                .execute();
    }
}
