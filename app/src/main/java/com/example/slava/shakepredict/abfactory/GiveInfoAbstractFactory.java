package com.example.slava.shakepredict.abfactory;


public abstract class GiveInfoAbstractFactory {
    public abstract FactInterface getFact();
    public abstract JokeInterface getJoke();
    public abstract PredictInterface getPredict();
}
