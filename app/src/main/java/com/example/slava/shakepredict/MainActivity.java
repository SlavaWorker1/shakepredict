package com.example.slava.shakepredict;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.activeandroid.ActiveAndroid;
import com.example.slava.shakepredict.abfactory.FactoryDirector;
import com.example.slava.shakepredict.factotymethod.FactoryMethod;
import com.example.slava.shakepredict.modelsdb.FactDb;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private SensorManager mSensorManager;
    private ShakeEventListener mSensorListener;
    String type = "";
    String infoType = "";
    RadioButton radioGood,radioBad, radioMessage;
    ImageView imgCreate;
    List<Message> messageList;
    RelativeLayout relWallpaper;

    public List<Message> getMessageList() {
        return messageList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
        );
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        messageList = new ArrayList<>();
        relWallpaper = (RelativeLayout)findViewById(R.id.relWallpaper);
        radioGood = (RadioButton)findViewById(R.id.radio_good);
        radioBad = (RadioButton)findViewById(R.id.radio_bad);
        radioMessage = (RadioButton)findViewById(R.id.radio_message);
        imgCreate = (ImageView)findViewById(R.id.imgCreate);
        imgCreate.setOnClickListener(this);
        imgCreate.setVisibility(View.INVISIBLE);


        ActiveAndroid.initialize(this);
        if(getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }


        final String arrPredict[] = {
                "Да",
                "Нет",
                "Неплохо, хороший план",
                "Звучит не плохо",
                "Любое утверждение ложно. Это — тоже!",
        };

        List<FactDb> factDbList = FactDb.getFactByType("good");
        String fact = factDbList.get(0).fact;
        Log.d("database", fact);

        ImageView imgView = (ImageView)findViewById(R.id.imgBall);

        Bitmap bmpBall = Utils.decodeSampledBitmapFromDrawable(
                this,
                R.drawable.magick_ball,
                imgView.getMaxWidth(),
                imgView.getMaxHeight()
        );

        imgView.setImageBitmap(bmpBall);


        final TextView tvPredict = (TextView)findViewById(R.id.tvPredict);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorListener = new ShakeEventListener();
        mSensorListener.setOnShakeListener(new ShakeEventListener.OnShakeListener() {

            public void onShake() {
                Random r = new Random();
                if(infoType.equals("Message")) {
                    if(!messageList.isEmpty()) {
                        Message message = messageList.get(r.nextInt(messageList.size()));
                        tvPredict.setText(
                                "Дорогой " + message.getCompanion() + ", \n" +
                                message.getMessage() + " С уважением " +
                                message.getAuthor() + " " +
                                message.getData()
                        );
                    } else {
                        tvPredict.setText("Вы ещё ничего не добавили");
                    }
                } else if(!type.isEmpty() && !infoType.isEmpty()) {
                    String info = new FactoryDirector().createFactory(type, infoType);
                    tvPredict.setText(info);
                    Log.d("Info", info);
                } else {
                    tvPredict.setText(arrPredict[r.nextInt(arrPredict.length)]);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(mSensorListener,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        mSensorManager.unregisterListener(mSensorListener);
        super.onPause();
    }


    public void onRadioButtonTypeClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.radio_bad:
                if (checked)
                    type = "bad";
                    break;
            case R.id.radio_good:
                if (checked)
                    type = "good";
                    break;
        }
    }

    public void onRadioButtonInfoTypeClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        imgCreate.setVisibility(View.INVISIBLE);
        switch(view.getId()) {
            case R.id.radio_fact:
                if (checked)
                    infoType = "Fact";
                break;
            case R.id.radio_joke:
                if (checked)
                    infoType = "Joke";
                break;
            case R.id.radio_predict:
                if (checked)
                    infoType = "Predict";
                break;
            case R.id.radio_message:
                if(checked) {
                    infoType = "Message";
                    radioBad.setChecked(false);
                    radioGood.setChecked(false);
                    type = "";
                    imgCreate.setVisibility(View.VISIBLE);
                }
        }
    }

    public void onRadioButtonWallpaperClicked(View view) {
        String wallpaper = null;
        switch (view.getId()) {
            case R.id.radio_Green:
                wallpaper = "Light green";
                break;
            case R.id.radio_Lime:
                wallpaper = "Lime";
                break;
        }
        new FactoryMethod().getWallppaperColor(wallpaper).setWallpaper(relWallpaper);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imgCreate:
                new DialogCreateMessage(this);
                break;
        }
    }
}
